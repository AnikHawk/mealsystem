package net.therap.meal.system.dao;

import net.therap.meal.system.entity.Item;
import net.therap.meal.system.service.DBConnectionService;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.NoResultException;
import javax.persistence.Query;
import java.util.List;

/**
 * @author user
 * @since 2/19/2020
 */
public class ItemDao {

    public static List<Object> getAll(){
        EntityManagerFactory emf = DBConnectionService.getEntityManagerFactory();
        EntityManager em = emf.createEntityManager();
        em.getTransaction().begin();

        Query query = em.createQuery("from Item", Item.class);
        List<Object> list = query.getResultList();

        return list;
    }

    public static void add(Item item) {
        EntityManagerFactory emf = DBConnectionService.getEntityManagerFactory();
        EntityManager em = emf.createEntityManager();
        em.getTransaction().begin();

        em.persist(item);

        em.getTransaction().commit();
        em.close();
        emf.close();
    }

    public static void remove(Item item) {
        EntityManagerFactory emf = DBConnectionService.getEntityManagerFactory();
        EntityManager em = emf.createEntityManager();
        em.getTransaction().begin();

        int id = fetchId(item);
        item = em.find(Item.class, id);
        em.remove(item);

        em.getTransaction().commit();
        em.close();
        emf.close();
    }

    public static void update(Item item, String itemNameAfter) {
        EntityManagerFactory emf = DBConnectionService.getEntityManagerFactory();
        EntityManager em = emf.createEntityManager();
        em.getTransaction().begin();

        int id = fetchId(item);
        item = em.find(Item.class, id);
        item.setName(itemNameAfter);

        em.getTransaction().commit();
        em.close();
        emf.close();
    }

    public static boolean exists(Item item) {
        if(fetchId(item) == -1){
            return false;
        }
        return true;
    }

    public static int fetchId(Item item) {
        EntityManagerFactory emf = DBConnectionService.getEntityManagerFactory();
        EntityManager em = emf.createEntityManager();

        Query query = em.createQuery("select item from Item item where item.name = :name", Item.class);
        query = query.setParameter("name", item.getName());

        Item fetchedItem;
        int id;

        try {
            fetchedItem = (Item) query.getSingleResult();
            id = fetchedItem.getId();
        } catch (NoResultException e) {
            id = -1;
        }

        return id;
    }
}
