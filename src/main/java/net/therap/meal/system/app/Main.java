package net.therap.meal.system.app;

import net.therap.meal.system.dao.ItemDao;
import net.therap.meal.system.entity.Item;

import java.util.List;

/**
 * @author altamas.haque
 * @since 2/19/20
 */
public class Main {
    public static void main(String[] args) {
        Item item = new Item();
        item.setName("Beef");
        ItemDao.add(item);

        List<Object> list = ItemDao.getAll();
        for (Object obj : list) {
            Item itm = (Item) obj;
            System.out.println(itm.getName());
        }
    }
}
