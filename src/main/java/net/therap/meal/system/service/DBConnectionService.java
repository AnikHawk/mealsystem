package net.therap.meal.system.service;

import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

/**
 * @author user
 * @since 2/19/2020
 */
public class DBConnectionService {
    private static EntityManagerFactory entityManagerFactory;

    public static EntityManagerFactory getEntityManagerFactory() {
        entityManagerFactory = Persistence.createEntityManagerFactory("emFactory");
        return entityManagerFactory;
    }
}
