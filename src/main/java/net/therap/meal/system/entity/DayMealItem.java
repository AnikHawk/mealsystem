package net.therap.meal.system.entity;

import javax.persistence.*;

/**
 * @author user
 * @since 2/19/2020
 */
@Entity
@Table(name = "daymealitem", uniqueConstraints = {@UniqueConstraint(columnNames = {"day_id", "item_id", "meal_id"})})
public class DayMealItem {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @ManyToOne(cascade = {CascadeType.PERSIST, CascadeType.MERGE})
    private Day day;

    @ManyToOne(cascade = {CascadeType.PERSIST, CascadeType.MERGE})
    private Meal meal;

    @ManyToOne(cascade = {CascadeType.PERSIST, CascadeType.MERGE})
    private Item item;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Day getDay() {
        return day;
    }

    public void setDay(Day day) {
        this.day = day;
    }

    public Meal getMeal() {
        return meal;
    }

    public void setMeal(Meal meal) {
        this.meal = meal;
    }

    public Item getItem() {
        return item;
    }

    public void setItem(Item item) {
        this.item = item;
    }
}
