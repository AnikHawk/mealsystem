package net.therap.meal.system.entity;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

/**
 * @author user
 * @since 2/19/2020
 */
@Entity
public class Day {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @Column(unique = true)
    private String name;

    @OneToMany(mappedBy = "day", cascade = {CascadeType.PERSIST, CascadeType.MERGE})
    private Set<DayMealItem> dayMealItems = new HashSet<>();

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Set<DayMealItem> getDayMealItems() {
        return dayMealItems;
    }

    public void setDayMealItems(Set<DayMealItem> dayMealItems) {
        this.dayMealItems = dayMealItems;
    }
}
